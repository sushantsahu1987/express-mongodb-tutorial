var express = require('express');
var app = express();
var url = 'mongodb://127.0.0.1:27017/mynewdb';
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;

MongoClient.connect(url,function(err,db) {

  if(err){
    console.log('unable to establish a connection '+err);
  }else {

    console.log('connection established ');

    var collection = db.collection('mynewcollections');

    collection.find().toArray(function(err,result) {

      if(err){
        console.log('An error has occured '+err);
      }else if(result.length) {
          console.log('result length '+result.length);
      }else {
        console.log('no records found');
      }

      db.close();

    });



  }


});
